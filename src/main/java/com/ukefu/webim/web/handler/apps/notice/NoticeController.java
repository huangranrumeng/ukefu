package com.ukefu.webim.web.handler.apps.notice;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.ukefu.core.UKDataContext;
import com.ukefu.util.Menu;
import com.ukefu.webim.service.repository.CallOutRoleRepository;
import com.ukefu.webim.service.repository.NoticeMsgRepository;
import com.ukefu.webim.service.repository.NoticeRepository;
import com.ukefu.webim.service.repository.NoticeTargetRepository;
import com.ukefu.webim.service.repository.OrganRepository;
import com.ukefu.webim.service.repository.UserRepository;
import com.ukefu.webim.service.repository.UserRoleRepository;
import com.ukefu.webim.web.handler.Handler;
import com.ukefu.webim.web.model.CallOutRole;
import com.ukefu.webim.web.model.Notice;
import com.ukefu.webim.web.model.NoticeMsg;
import com.ukefu.webim.web.model.NoticeTarget;
import com.ukefu.webim.web.model.Organ;
import com.ukefu.webim.web.model.User;
import com.ukefu.webim.web.model.UserRole;

/**
 * 公告信息-公告管理
 *
 */
@Controller
@RequestMapping("/apps/notice")
public class NoticeController extends Handler{

	@Autowired
	private NoticeRepository noticeRes ;
	
	@Autowired
	private NoticeTargetRepository noticeTargetRes ;
	
	@Autowired
	private UserRepository userRes ;
	
	@Autowired
	private UserRoleRepository userRoleRes; // 查询角色-用户关联

	@Autowired
	private CallOutRoleRepository callOutRoleRes;// 角色授权部门
	
	@Autowired
	private OrganRepository organRes;// 部门
	
	@Autowired
	private NoticeMsgRepository noticeMsgRes;//消息
	
	@RequestMapping("/index")
	@Menu(type = "notice", subtype = "notice")
	public ModelAndView index(ModelMap map , HttpServletRequest request ,HttpServletResponse response ,@Valid String msg) {
		final String orgi = super.getOrgi(request);
		Page<Notice> noticeList = noticeRes.findAll(new Specification<Notice>(){
			@Override
			public Predicate toPredicate(Root<Notice> root, CriteriaQuery<?> query,CriteriaBuilder cb) {
				List<Predicate> list = new ArrayList<Predicate>();  

				list.add(cb.equal(root.get("orgi").as(String.class), orgi));

				Predicate[] p = new Predicate[list.size()];  
				return cb.and(list.toArray(p));   
			}}, new PageRequest(super.getP(request), super.getPs(request), Sort.Direction.DESC, new String[] { "createtime" }));
		
		
		map.addAttribute("noticeList",noticeList) ;
		map.addAttribute("msg",msg) ;
		map.addAttribute("userList",userRes.findByOrgi(orgi)) ;
		return request(super.createAppsTempletResponse("/apps/notice/index")) ;
	}
	
	@RequestMapping(value = "/add")
    @Menu(type = "notice" , subtype = "notice")
    public ModelAndView add(ModelMap map , HttpServletRequest request ) {
		return request(super.createAppsTempletResponse("/apps/notice/add"));
    }
	
	@RequestMapping(value = "/add/save")
    @Menu(type = "notice" , subtype = "notice")
    public ModelAndView addsave(ModelMap map , HttpServletRequest request ,@Valid Notice notice , @RequestParam(value = "files", required = false) MultipartFile[] files)  throws Exception {
		final User user = super.getUser(request) ;
		String msg="add_failure";
		if (notice != null) {
			notice.setCreater(user.getId());
			notice.setCreatetime(new Date());
			notice.setOrgi(super.getOrgi(request));
			noticeRes.save(notice) ;
			msg = "add_success" ;
		}
		return request(super.createRequestPageTempletResponse("redirect:/apps/notice/index.html?msg="+msg));
    }
	
	@RequestMapping(value = "/edit")
    @Menu(type = "notice" , subtype = "notice")
    public ModelAndView edit(ModelMap map , HttpServletRequest request ,@Valid String id) {
		if (!StringUtils.isBlank(id)) {
			Notice notice = noticeRes.findByIdAndOrgi(id, super.getOrgi(request));
			if (notice != null) {
				map.addAttribute("notice",notice) ;
			}
		}
    	return request(super.createAppsTempletResponse("/apps/notice/edit"));
    }
	
	@RequestMapping(value = "/edit/save")
    @Menu(type = "notice" , subtype = "notice")
    public ModelAndView editsave(ModelMap map , HttpServletRequest request , @Valid String id ,@Valid Notice notice ) {
		String msg="edit_failure";
		if (!StringUtils.isBlank(id)) {
			Notice notic = noticeRes.findByIdAndOrgi(id, super.getOrgi(request));
			if (notic != null) {
				notic.setContent(notice.getContent());
				notic.setKeyword(notice.getKeyword());
				notic.setSummary(notice.getSummary());
				notic.setTags(notice.getTags());
				notic.setTitle(notice.getTitle());
				notic.setUpdatetime(new Date());
				noticeRes.save(notic) ;
				msg = "edit_success" ;
			}
		}
		return request(super.createRequestPageTempletResponse("redirect:/apps/notice/detail.html?id="+id+"&msg="+msg));
    }
	
	@RequestMapping("/delete")
    @Menu(type = "notice" , subtype = "notice" )
    public ModelAndView delete(ModelMap map , HttpServletRequest request , @Valid String id) throws SQLException {
		String msg="del_failure";
		if (!StringUtils.isBlank(id)) {
			Notice notice = noticeRes.findByIdAndOrgi(id, super.getOrgi(request)) ;
			if (notice != null) {
				List<NoticeTarget> noticeTargetList = noticeTargetRes.findByNoticeidAndOrgi(id, super.getOrgi(request)) ;
				if (noticeTargetList != null && noticeTargetList.size() > 0) {
					noticeTargetRes.delete(noticeTargetList);
				}
				noticeRes.delete(notice);
				msg="del_success";
			}
		}
    	return request(super.createRequestPageTempletResponse("redirect:/apps/notice/msg/index.html?msg="+msg+"&p="+super.getP(request)));
    }
	
	@RequestMapping("/batdelete")
    @Menu(type = "notice" , subtype = "notice" )
    public ModelAndView batdelete(ModelMap map , HttpServletRequest request , @Valid String[] ids) throws SQLException {
    	String msg = "bat_delete_success";
    	if(ids!=null && ids.length>0){
    		List<NoticeTarget> noticeTargetList = new ArrayList<NoticeTarget>() ;
    		for(String id : ids) {
    			List<NoticeTarget> nTargetList = noticeTargetRes.findByNoticeidAndOrgi(id, super.getOrgi(request)) ;
    			if (nTargetList != null && nTargetList.size()> 0) {
    				noticeTargetList.addAll(nTargetList) ;
				}
    		}
			if (noticeTargetList != null && noticeTargetList.size() > 0) {
				noticeTargetRes.delete(noticeTargetList);
			}
    		noticeRes.delete(noticeRes.findAll(Arrays.asList(ids)) );
    	}else {
    		msg = "bat_delete_faild";
    	}
    	return request(super.createRequestPageTempletResponse("redirect:/apps/notice/index.html?msg="+msg));
    }
	
	@RequestMapping("/detail")
    @Menu(type = "notice" , subtype = "notice" )
    public ModelAndView detail(ModelMap map , HttpServletRequest request , @Valid String id , @Valid String msg) throws SQLException {
		if (!StringUtils.isBlank(id)) {
			Notice notice = noticeRes.findByIdAndOrgi(id, super.getOrgi(request));
			if (notice != null) {
				map.addAttribute("notice",notice) ;
			}
		}
		map.addAttribute("msg",msg) ;
    	return request(super.createAppsTempletResponse("/apps/notice/detail"));
    }
	
	/**
	 * 	发送弹窗
	 */
	@RequestMapping("/sendlist")
    @Menu(type = "notice" , subtype = "notice" )
    public ModelAndView sendlist(ModelMap map , HttpServletRequest request , @Valid String id,  @Valid String organ ,@Valid String noticeid) throws SQLException {
		map.addAttribute("userList",userRes.findByOrgi(super.getOrgi(request))) ;
//		final List<String> organList = getAuthOrgan(userRoleRes, callOutRoleRes, super.getUser(request));
//    	List<Organ> tempOrganList = organRes.findAll(organList) ;
		map.addAttribute("organList", organRes.findAll());
		map.addAttribute("userid", super.getUser(request).getId());
		map.addAttribute("noticeid", noticeid);
		if (!StringUtils.isBlank(noticeid)) {
			map.addAttribute("noticeTargetList", noticeTargetRes.findByNoticeidAndCheckedAndOrgi(noticeid,true, super.getOrgi(request)));
		}
    	return request(super.createRequestPageTempletResponse("/apps/notice/sendlist"));
    }
	
	/**
	 * 	  发送弹窗-搜索
	 */
	@RequestMapping("/sendlist/search")
    @Menu(type = "notice" , subtype = "notice" )
    public ModelAndView search(ModelMap map , HttpServletRequest request , @Valid String stype,@Valid final String q , @Valid final String organ, @Valid final String noticeid) throws SQLException {
		final String orgi = super.getOrgi(request) ;
		ModelAndView view = request(super.createRequestPageTempletResponse("/apps/notice/include/userlist")) ; ;
		if ("agent".equals(stype)) {
			List<User> userList = userRes.findAll(new Specification<User>(){
				@Override
				public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query,CriteriaBuilder cb) {
					List<Predicate> list = new ArrayList<Predicate>();  

					list.add(cb.equal(root.get("orgi").as(String.class), orgi));
					if (!StringUtils.isBlank(organ)) {
						if ("0".equals(organ)) {
							list.add(cb.isNotNull(root.get("organ").as(String.class)));
						}else {
							list.add(cb.equal(root.get("organ").as(String.class), organ));
						}
					}
					if (!StringUtils.isBlank(q)) {
						list.add(cb.or(cb.like(root.get("uname").as(String.class), "%"+q+"%"), cb.like(root.get("username").as(String.class), "%"+q+"%")));

					}
					Predicate[] p = new Predicate[list.size()];  
					return cb.and(list.toArray(p));   
				}});
			map.addAttribute("userList",userList) ;
		}else if ("organ".equals(stype)) {
			List<Organ> organList = organRes.findAll(new Specification<Organ>(){
				@Override
				public Predicate toPredicate(Root<Organ> root, CriteriaQuery<?> query,CriteriaBuilder cb) {
					List<Predicate> list = new ArrayList<Predicate>();  

					list.add(cb.equal(root.get("orgi").as(String.class), orgi));
					if (!StringUtils.isBlank(q)) {
						list.add(cb.like(root.get("name").as(String.class), "%"+q+"%"));

					}
					Predicate[] p = new Predicate[list.size()];  
					return cb.and(list.toArray(p));   
				}});
			map.addAttribute("organlist",organList) ;
			view = request(super.createRequestPageTempletResponse("/apps/notice/include/organlist")) ;
		}
		if (!StringUtils.isBlank(organ) && !"0".equals(organ)) {
			Organ currentorgan = organRes.findByIdAndOrgi(organ, super.getOrgi(request)) ;
			if (currentorgan != null) {
				map.addAttribute("currentorgan",currentorgan) ;
			}
		}
		map.addAttribute("q",q) ;
		if (!StringUtils.isBlank(noticeid)) {
			map.addAttribute("noticeTargetList", noticeTargetRes.findByNoticeidAndCheckedAndOrgi(noticeid,true, super.getOrgi(request)));
		}
		
    	return view;
    }
	
	/**
	 * 	发送弹窗-选中事件
	 */
	@RequestMapping("/sendlist/checkitem")
    @Menu(type = "notice" , subtype = "notice" )
    public ModelAndView checkitem(ModelMap map , HttpServletRequest request , @Valid String target,  @Valid boolean ischecked ,@Valid String noticeid , @Valid String[] targets) throws SQLException {
		final String orgi = super.getOrgi(request) ;
		if (!StringUtils.isBlank(noticeid)) {
			if (!StringUtils.isBlank(target)) {
				NoticeTarget noticeTarget = noticeTargetRes.findByNoticeidAndTargetAndOrgi(noticeid,target, orgi) ;
				if (noticeTarget != null) {
					noticeTarget.setChecked(ischecked);
					noticeTargetRes.save(noticeTarget) ;
				}else {
					noticeTarget = new NoticeTarget();
					noticeTarget.setChecked(ischecked);
					noticeTarget.setCreater(super.getUser(request).getId());
					noticeTarget.setCreatetime(new Date());
					noticeTarget.setNoticeid(noticeid);
					noticeTarget.setOrgi(orgi);
					noticeTarget.setTarget(target);
					noticeTargetRes.save(noticeTarget) ;
				}
			}else if (targets != null && targets.length > 0) {
				List<NoticeTarget> ntList = new ArrayList<NoticeTarget>() ;
		    	for(String tid : targets) {
		    		NoticeTarget nt = noticeTargetRes.findByNoticeidAndTargetAndOrgi(noticeid, tid, orgi) ;
		    		if (nt != null) {
		    			nt.setChecked(ischecked);
					}else {
						nt = new NoticeTarget();
						nt.setChecked(ischecked);
						nt.setCreater(super.getUser(request).getId());
						nt.setCreatetime(new Date());
						nt.setNoticeid(noticeid);
						nt.setOrgi(orgi);
						nt.setTarget(tid);
					}
		    		ntList.add(nt) ;
		    	}
		    	if (ntList != null && ntList.size() > 0) {
		    		noticeTargetRes.save(ntList) ;
				}
			}
			
		}
		return null ;
    }
	
	/**
	 * 	发送弹窗-发送
	 */
	@RequestMapping("/sendlist/save")
    @Menu(type = "notice" , subtype = "notice" )
    public ModelAndView sendlistsave(ModelMap map , HttpServletRequest request ,@Valid String noticeid,@Valid boolean sysms ,@Valid boolean sms ,@Valid boolean mail) throws SQLException {
		User user = super.getUser(request) ;
		String orgi = super.getOrgi(request) ;
		String msg = "send_faild" ;
		if(sysms) {
			if (!StringUtils.isBlank(noticeid)) {
				List<NoticeMsg> noticeMsgList = new ArrayList<NoticeMsg>() ;
				Notice notice = noticeRes.findByIdAndOrgi(noticeid, orgi) ;
				if (notice != null) {
					if (!StringUtils.isBlank(notice.getTitle())) {
						List<NoticeTarget> noticeTargetList = noticeTargetRes.findByNoticeidAndCheckedAndOrgi(noticeid, true, orgi) ;
						if (noticeTargetList != null && noticeTargetList.size() > 0) {
							for(NoticeTarget nt : noticeTargetList) {
								if (nt != null) {
									NoticeMsg mmsg = new NoticeMsg() ;
									mmsg.setContent(notice.getContent());
									mmsg.setCreater(user.getId());
									mmsg.setCreaterusername(user.getUname());
									mmsg.setCreatetime(new Date());
									mmsg.setDatastatus(false);
									mmsg.setTitle(notice.getTitle());
									mmsg.setOrgi(orgi);
									mmsg.setStatus(false);
									mmsg.setTarget(nt.getTarget());
									noticeMsgList.add(mmsg) ;
								}
							}
						}
					}
				}
				if (noticeMsgList != null && noticeMsgList.size() > 0) {
					msg = "send_success" ;
					noticeMsgRes.save(noticeMsgList) ;
				}
			}
		}
		if (sms) {
			System.out.println("sms");
		}
		if (mail) {
			System.out.println("mail");
		}
    	return request(super.createRequestPageTempletResponse("redirect:/apps/notice/detail.html?id="+noticeid+"&msg="+msg));
    }
	
	/**
	 * 我的部门以及授权给我的部门
	 * @param userRoleRes
	 * @param callOutRoleRes
	 * @param user
	 * @return
	 */
	public static List<String> getAuthOrgan(UserRoleRepository userRoleRes , CallOutRoleRepository callOutRoleRes,User user){
		List<UserRole> userRole = userRoleRes.findByOrgiAndUser(user.getOrgi(), user);
		ArrayList<String> organList = new ArrayList<String>();
		if (userRole.size() > 0) {
			for (UserRole userTemp : userRole) {
				CallOutRole roleOrgan = callOutRoleRes.findByOrgiAndRoleid(user.getOrgi(),
						userTemp.getRole().getId());
				if (roleOrgan != null) {
					if (!StringUtils.isBlank(roleOrgan.getOrganid())) {
						String[] organ = roleOrgan.getOrganid().split(",");
						for (int i = 0; i < organ.length; i++) {
							organList.add(organ[i]);
						}
						
					}
				}
			}
		}
		
		if(user!=null && !StringUtils.isBlank(user.getOrgan())) {
			organList.add(user.getOrgan()) ;
		}
		return organList ;
	}
}
